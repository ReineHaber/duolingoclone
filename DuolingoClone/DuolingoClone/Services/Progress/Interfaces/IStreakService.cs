﻿using DuolingoClone.Entities.Progress;
using MongoDB.Driver.Core.Operations;

namespace DuolingoClone.Services.Progress.Interfaces
{
    public interface IStreakService
    {
        public string CreateStreak(string userId);
        public bool Haslearned(DateTime date,  string userId);
        public List<Streak> GetAllStreaks(string userId);
    }
}
