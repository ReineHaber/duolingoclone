﻿using DuolingoClone.Entities.Progress;
using DuolingoClone.Entities.Progress.DTOs;

namespace DuolingoClone.Services.Progress.Interfaces
{
    public interface ICourseService
    {
        public string AddCourse(string userId, string langId);
        public EnrolledCourses GetCourse(string userId, string language);
        public void AddLevelProgress(string courseId, string levelId);
        public string UpdateXp(string courseId, int xp);
        public string UpdateStatus(string courseId);
        List<levelAvailableDTO> GetLevels(string userId , string LangId);
        public Dictionary<string, string> GetAllEnrolledCourses(string userId);
        public string ChallengeCompleted(string levelId, string courseId);
    }
}
