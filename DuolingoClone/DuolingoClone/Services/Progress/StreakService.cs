﻿using DuolingoClone.Entities.Progress;
using DuolingoClone.Repositories.Progress.Interfaces;
using DuolingoClone.Services.Progress.Interfaces;

namespace DuolingoClone.Services.Progress
{
    public class StreakService : IStreakService
    {
        private IStreakRepository streakrepository;

        public StreakService(IStreakRepository _streakRepository)
        {
            streakrepository = _streakRepository;
        }

        public string CreateStreak(string userId)
        {
            Streak streak = new()
            {
                UserId = userId
            };

            string message = streakrepository.AddStreak(streak);
            return message;
        }

        public bool Haslearned(DateTime date, string userId)
        {
            bool haslearned = streakrepository.HasLearned(date, userId);
            return haslearned;
        }

        public List<Streak> GetAllStreaks(string userId)
        {
            List<Streak> streaks = streakrepository.GetStreaks(userId);
            return streaks;
        }
    }
}
