﻿using DuolingoClone.Entities.Progress;
using DuolingoClone.Repositories.Progress.Interfaces;
using DuolingoClone.Repositories.Lang.Interfaces;
using DuolingoClone.Services.Progress.Interfaces;
using DuolingoClone.Entities.Lang;
using DuolingoClone.Repositories.Sections;
using DuolingoClone.Repositories.Sections.Intefaces;
using DuolingoClone.Repositories.Progress;
using DuolingoClone.Entities.Progress.DTOs;

namespace DuolingoClone.Services.Progress
{
    public class CourseService : ICourseService
    {
        private ICourseRepository courseRepository;
        private ILanguageRepository languageRepository;
        private ILevelRepository levelRepository;

        public CourseService(ICourseRepository _courseRepository, ILanguageRepository _languageRepository, ILevelRepository _levelRepository)
        {
            courseRepository = _courseRepository;
            languageRepository = _languageRepository;
            levelRepository = _levelRepository;
        }

        public string AddCourse(string userId, string langId)
        {
            EnrolledCourses course = new()
            {
                UserId = userId,
                LanguageId = langId
            };
            string id = courseRepository.AddEnrolledCourse(course);
            string levelId = levelRepository.GetLevelId(langId, 1);
            AddLevelProgress(id, levelId);
            return id; 
        }

        public void AddLevelProgress(string courseId, string levelId)
        {
            LevelProgress level = new()
            {
                EnrolledCourseId = courseId,
                LevelId = levelId
            };
           
            courseRepository.AddLevelProgress(level);
        }

        public string UpdateStatus(string courseId)
        {
            courseRepository.UpdateStatus(courseId);
            return "deleted!";
        }

        public string UpdateXp(string courseId, int xp)
        {
            courseRepository.UpdateXp(courseId, xp);
            return "Xp added!";
        }

        public List<levelAvailableDTO> GetLevels(string userId , string LangId)
        {
            List<LevelProgress> levels = courseRepository.GetAvailableLevels(userId , LangId);
            List<levelAvailableDTO> activelevels = new List<levelAvailableDTO>();
            foreach(LevelProgress level in levels)
            {
                levelAvailableDTO levelAvailable = new()
                {
                    LevelId = level.LevelId,
                    CurrentChallengeNbr = level.CurrentChallengeNbr,
                    IsFinished = level.IsFinished
                };
                activelevels.Add(levelAvailable);
            }
            return activelevels;
        }

        public EnrolledCourses GetCourse(string userId , string language)
        {
            string id = languageRepository.GetLanguageId(language);
            return courseRepository.GetCourse(userId, id);
        }

        public Dictionary<string, string> GetAllEnrolledCourses(string userId)
        {
            List<EnrolledCourses> list = courseRepository.GetAllEnrolledCourses(userId);
            Dictionary<string, string> courses = new Dictionary<string, string>();

            foreach(EnrolledCourses course in list)
            {
                Language language = languageRepository.GetLanguageById(course.LanguageId);
                courses[language.LanguageId] = language.LanguageName;
            }

            return courses;
        }

        public string ChallengeCompleted(string levelId, string courseId)
        {
            LevelProgress levelProgress = courseRepository.GetLevelProgressById(levelId);
            if (levelProgress.CurrentChallengeNbr == 5)
            {
                levelProgress.IsFinished = true;
                string nextLevelId = levelRepository.GetLevelId(levelId);
                LevelProgress newLevelProgress = new()
                {
                    LevelId = nextLevelId,
                    EnrolledCourseId = courseId
                };
                courseRepository.AddLevelProgress(levelProgress);
                return "new level progress";
            }

            levelProgress.CurrentChallengeNbr += 1;

            return courseRepository.ChallengeCompleted(levelProgress);
        }


    
    }
}
