﻿namespace DuolingoClone.Services
{
    public class ItemExistsOrIsNull : Exception
    {
        public ItemExistsOrIsNull(string message): base(message) 
        {

        }
    }
}
