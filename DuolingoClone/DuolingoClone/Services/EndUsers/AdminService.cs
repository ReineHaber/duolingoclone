﻿using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Entities.EndUsers.DTOs;
using DuolingoClone.Repositories.EndUsers;
using DuolingoClone.Repositories.EndUsers.Intefaces;
using DuolingoClone.Services.EndUsers.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace DuolingoClone.Services.EndUsers
{
    public class AdminService : IAdminService
    {
        private IAdminRepository adminRepository;
        private ITokenService tokenService;
        private ILoginRepository loginRepository;

        public AdminService(IAdminRepository _adminRepository, ILoginRepository _loginRepository, ITokenService _tokenService)
        {
            adminRepository = _adminRepository;
            tokenService = _tokenService;
            loginRepository = _loginRepository;
        }


        public string GetAdmin(string username, string password)
        {
            var hashedPassword = EncryptionUtil.Encryption(password);
            Admin admin = loginRepository.LoginUser<Admin>(username, hashedPassword);

            if(admin is null)
            {
                throw new ItemExistsOrIsNull("admin not found");
            }


            var payload = new JwtPayload
           {
               {"Id",admin.UserId },
               {"UserName" , admin.UserName},
               {"Role","admin" }
           };

            string token = tokenService.CreateToken(payload);
            return admin.UserName ;
        }

        public string CreateAdmin(UserDTO userDTO)
        {
            Admin admin = new()
            {
                UserName = userDTO.UserName,
                Email = userDTO.Email,
                Password = EncryptionUtil.Encryption(userDTO.Password)
            };

            string id = adminRepository.AddUser(admin);

            if (id is null)
            {
                throw new ItemExistsOrIsNull("could not create");
            }

            return id;
        }
    }
}
