﻿using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Services.EndUsers.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace DuolingoClone.Services.EndUsers
{
    public class TokenService : ITokenService 
    {
        public string CreateToken(JwtPayload payload)
        {
            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8
                .GetBytes("thesecretkey3rc3sharedkeythisismycustomSecretkeyforauthentication"));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var header = new JwtHeader(creds);
            var token = new JwtSecurityToken(header, payload);

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt;
        }

    }
}
