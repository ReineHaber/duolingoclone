﻿using System.Security.Cryptography;
using System.Text;

namespace DuolingoClone.Services.EndUsers
{
    public class EncryptionUtil
    {
        public static string Encryption(string password)
        {
            var sha = SHA256.Create();
            var asByteArray = Encoding.Default.GetBytes(password);
            var PasswordHash = sha.ComputeHash(asByteArray);
            return Convert.ToBase64String(PasswordHash);
        }
    }
}
