﻿using System.IdentityModel.Tokens.Jwt;

namespace DuolingoClone.Services.EndUsers.Interfaces
{
    public interface ITokenService
    {
        public string CreateToken(JwtPayload payload);
    }
}
