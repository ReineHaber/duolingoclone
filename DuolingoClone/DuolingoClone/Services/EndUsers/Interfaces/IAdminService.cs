﻿using DuolingoClone.Entities.EndUsers.DTOs;

namespace DuolingoClone.Services.EndUsers.Interfaces
{
    public interface IAdminService
    {
        public string GetAdmin(string username, string password);
        public string CreateAdmin(UserDTO userDTO);
    }
}
