﻿using DuolingoClone.Entities.EndUsers.DTOs;

namespace DuolingoClone.Services.EndUsers.Interfaces
{
    public interface IStudentService
    {
        public string CreateStudent(UserDTO userDTO);
        public string GetStudent(string username, string password);
    }
}
