﻿using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Entities.EndUsers.DTOs;
using DuolingoClone.Repositories.EndUsers.Intefaces;
using DuolingoClone.Services.EndUsers.Interfaces;
using System.Globalization;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace DuolingoClone.Services.EndUsers
{
    public class StudentService : IStudentService
    {

        private IStudentRepository studentRepository;
        private ILoginRepository loginRepository;
        private ITokenService tokenService;


        public StudentService(IStudentRepository _studentRepository, ILoginRepository _loginRepository, ITokenService _tokenService)
        {
            studentRepository = _studentRepository;
            loginRepository = _loginRepository;
            tokenService = _tokenService;   
        }


        public string CreateStudent(UserDTO userDTO)
        {

            if (studentRepository.EmailExist(userDTO.Email))
            {
                throw new ItemExistsOrIsNull("email already exist!");
            }

            Student student = new()
            {
                UserName = userDTO.UserName,
                Email = userDTO.Email,
                Password = EncryptionUtil.Encryption(userDTO.Password),
            };

            string userid = studentRepository.AddUser(student);

            if(userid == null)
            {
                throw new ItemExistsOrIsNull("couldn't insert");
            }

            var payload = new JwtPayload
            {
               {"Id",student.UserId },
               {"UserName" , student.UserName}
            };

            string token = tokenService.CreateToken(payload);
            return userid;
        }


        public string GetStudent(string username, string password)
        {

            var hashedPassword = EncryptionUtil.Encryption(password);
            Student? student = loginRepository.LoginUser<Student>(username, hashedPassword);

            if (student is null)
            {
                throw new ItemExistsOrIsNull("student not found!");
            }

            var payload = new JwtPayload
            {
               {"Id",student.UserId },
               {"UserName" , student.UserName}
            };

            string token = tokenService.CreateToken(payload);
            return token;
        }
    }
}
