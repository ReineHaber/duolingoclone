﻿using DuolingoClone.Repositories.Lang.Interfaces;
using DuolingoClone.Services.Lang.Interfaces;
using DuolingoClone.Entities.Lang;
using DuolingoClone.Services.Sections.Intefaces;

namespace DuolingoClone.Services.Lang
{
    public class LanguageService : ILanguageService
    {
        private ILanguageRepository languageRepository;
        private ILevelService levelService;


        public LanguageService(ILanguageRepository _languageRepository, ILevelService _levelService)
        {
            languageRepository = _languageRepository;
            levelService = _levelService;
        }



        public List<Language> GetAll()
        {
            List<Language> list = languageRepository.GetAllLanguages();

            return list;
        }


        public string AddLanguage(string lanName)
        { 

            Language language = new()
            {
                LanguageName = lanName
            };

            string langId = languageRepository.AddLanguage(language);
            if (langId == null)
            {
                return string.Empty;
            } 
            for (int i=0; i < 5; i++)
            {
                levelService.AddLevel(i + 1, langId);
            }
            return langId;
        }

        public Language GetLanguageById(string languageId)
        {
           return languageRepository.GetLanguageById(languageId);
        }
    }
}
