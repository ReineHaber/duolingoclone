﻿
using DuolingoClone.Entities.Lang;
using DuolingoClone.Repositories.Lang.Interfaces;
using DuolingoClone.Services.Lang.Interfaces;

namespace DuolingoClone.Services.Lang
{
    public class WordService : IWordService
    {
        private IWordRepository wordRepository;

        public WordService(IWordRepository _wordRepository)
        {
            wordRepository = _wordRepository;
        }

        public async void AddWords(IFormFile file , string LangId)
        {
            var filePath = Path.GetTempFileName();

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            var lines = File.ReadAllLines(filePath);
            
            foreach (var line in lines)
            {
                var parts = line.Split(':');
                var wordName = parts[0];
                var translation = parts[1];

                Word word = new()
                {
                    WordName = wordName.Trim(),
                    TranslationToEng = translation.Trim(),
                    LanguageId = LangId
                };
                wordRepository.Add(word);
            }
            File.Delete(filePath);
        }

        public string GetTranslation(string wordName)
        {
            Word word =  wordRepository.GetWordTranslation(wordName);
            if (word == null)
            {
                return string.Empty;
            }
            return word.TranslationToEng;
        }
    }
}
