﻿using DuolingoClone.Entities.Lang;

namespace DuolingoClone.Services.Lang.Interfaces
{
    public interface ILanguageService
    {
        public List<Language> GetAll();

        public string AddLanguage(string lanName);
        public Language GetLanguageById(string languageId);
    }
}
