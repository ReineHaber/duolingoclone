﻿namespace DuolingoClone.Services.Lang.Interfaces
{
    public interface IWordService
    {
        public  void AddWords(IFormFile file, string LangId);
        public string GetTranslation(string wordName);
    }
}
