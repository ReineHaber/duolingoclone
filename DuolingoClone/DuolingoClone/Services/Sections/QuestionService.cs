﻿using DuolingoClone.Entities.Sections;
using DuolingoClone.Repositories.Sections.Intefaces;
using DuolingoClone.Services.Sections.Intefaces;

namespace DuolingoClone.Services.Sections
{
    public class QuestionService : IQuestionService
    {
        private IQuestionRepository questionRepository;

        public QuestionService(IQuestionRepository _questionRepository)
        {
            questionRepository = _questionRepository;
        }

        public string AddQuestion(string questionText, string answer, string challengeId)
        {
            Question question = new()
            {
                QuestionText = questionText,
                QuestionAnswer = answer,
                ChallengeId = challengeId
            };
            return questionRepository.AddQuestion(question);
        }

        public List<Question> GetQuestions(string challengId)
        {
            return questionRepository.GetAllQuestions(challengId);
        }



    }
}
