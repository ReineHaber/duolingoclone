﻿using DuolingoClone.Entities.Sections;
using DuolingoClone.Repositories.Sections.Intefaces;
using DuolingoClone.Services.Sections.Intefaces;

namespace DuolingoClone.Services.Sections
{
    public class LevelService : ILevelService
    {

        private ILevelRepository levelRepository;
        private IChallengeService challengeService;


        public LevelService(ILevelRepository _levelRepository , IChallengeService _challengeService)
        {
            levelRepository = _levelRepository;
            challengeService = _challengeService;
        }




        public string AddLevel (int levelNbr, string langId)
        {
            string levelId;
            if(levelNbr == 0)
            {
                Level level1 = new()
                {
                    LevelOrderNbr = levelNbr,
                    LanguageId = langId,
                    LevelOrderRequired = null
                };
                levelId =  levelRepository.AddLevel(level1);
            }

            string id = GetLevelId(langId, levelNbr-1);
            Level level = new()
            {
                LevelOrderNbr = levelNbr,
                LanguageId = langId,
                LevelOrderRequired = id
            };

            levelId = levelRepository.AddLevel(level);

            for(int i =0; i < 5; i++)
            {
                challengeService.AddChallenge(levelId, i + 1);
            }

            return levelId;
        }




        public string GetLevelId (string langId, int ordernbr) 
        {
            return levelRepository.GetLevelId(langId, ordernbr);
        }




        public List<Level> GetLevels(string LangId)
        {
            return levelRepository.GetLevels(LangId);
        }
    }
}
