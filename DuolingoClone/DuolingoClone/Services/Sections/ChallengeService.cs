﻿using DuolingoClone.Entities.Sections;
using DuolingoClone.Repositories.Sections.Intefaces;
using DuolingoClone.Services.Sections.Intefaces;

namespace DuolingoClone.Services.Sections
{
    public class ChallengeService : IChallengeService
    {

        private IChallengeRepository challengeRepository;


        public ChallengeService(IChallengeRepository _challengeRepository)
        {
            challengeRepository = _challengeRepository;
        }


        public string AddChallenge(string levelId, int challengeNbr)
        {
            Challenge challenge = new()
            { 
                LevelId = levelId,
                ChallengeNbr = challengeNbr
            };

            string id =  challengeRepository.AddChallenge(challenge);

            if (id == null)
            {
                throw new ItemExistsOrIsNull("couldn't add the challenge");
            }
            return id;
        }

        public string GetChallengeId(string levelId, int challengeNbr) 
        {
            string id = challengeRepository.GetChallengeId(challengeNbr, levelId);
            if (id == null)
            {
                throw new ItemExistsOrIsNull("challenge not found");
            }
            return id;
        }

        public List<Challenge> getAll(string levelId)
        {
            List<Challenge> all = challengeRepository.getAll(levelId);
            if(all == null)
            {
                return null;
            }
            return all;
        }

    }
}
