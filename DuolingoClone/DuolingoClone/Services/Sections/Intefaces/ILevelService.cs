﻿using DuolingoClone.Entities.Sections;

namespace DuolingoClone.Services.Sections.Intefaces
{
    public interface ILevelService
    {
        public string GetLevelId(string langId, int ordernbr);
        public string AddLevel(int levelNbr, string langId);
        public List<Level> GetLevels(string LangId);
    }
}
