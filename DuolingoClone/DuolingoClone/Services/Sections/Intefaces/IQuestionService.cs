﻿using DuolingoClone.Entities.Sections;

namespace DuolingoClone.Services.Sections.Intefaces
{
    public interface IQuestionService
    {
        public List<Question> GetQuestions(string challengId);
        public string AddQuestion(string questionText, string answer, string challengeId);
    }
}
