﻿using DuolingoClone.Entities.Sections;

namespace DuolingoClone.Services.Sections.Intefaces
{
    public interface IChallengeService
    {
        public string AddChallenge(string levelId, int challengeNbr);
        public string GetChallengeId(string levelId, int challengeNbr);
        public List<Challenge> getAll(string levelId);
    }
}
