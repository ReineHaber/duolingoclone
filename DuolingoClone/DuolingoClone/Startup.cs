﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using DuolingoClone.Repositories.EndUsers;
using MongoDB.Driver;
using DuolingoClone.Database;
using DuolingoClone.Repositories.EndUsers.Intefaces;
using DuolingoClone.Services.EndUsers.Interfaces;
using DuolingoClone.Services.EndUsers;
using DuolingoClone.Repositories.Lang.Interfaces;
using DuolingoClone.Repositories.Lang;
using DuolingoClone.Services.Lang.Interfaces;
using DuolingoClone.Services.Lang;
using DuolingoClone.Repositories.Progress;
using DuolingoClone.Repositories.Progress.Interfaces;
using DuolingoClone.Services.Progress.Interfaces;
using DuolingoClone.Services.Progress;
using DuolingoClone.Repositories.Sections.Intefaces;
using DuolingoClone.Services.Sections.Intefaces;
using DuolingoClone.Repositories.Sections;
using DuolingoClone.Services.Sections;

namespace DuolingoClone
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public MongoDBContext db { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            
        }

        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("MongoDB");

            services.AddControllers();
            services.AddRazorPages();
            services.AddSingleton<MongoDBContext>(new MongoDBContext(connectionString,"Duolingo_Clone"));
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<IAdminRepository, AdminRepository>();
            services.AddScoped<IStudentRepository, StudentRepository>();
            services.AddScoped<IStudentService, StudentService>();
            services.AddScoped<ILanguageRepository, LanguageRepository>();
            services.AddScoped<ILanguageService, LanguageService>();
            services.AddScoped<IWordRepository, WordRepository>();
            services.AddScoped<IWordService, WordService>();
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<IStreakRepository, StreakRepository>();
            services.AddScoped<IStreakService, StreakService>();
            services.AddScoped<IChallengeRepository, ChallengeRepository>();
            services.AddScoped<IChallengeService, ChallengeService>();
            services.AddScoped<ILevelRepository, LevelRepository>();
            services.AddScoped<ILevelService, LevelService>();
            services.AddScoped<IQuestionRepository, QuestionRepository>();
            services.AddScoped<IQuestionService, QuestionService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<ILoginRepository, LoginRepository>();

            services.AddLogging(logging =>
            {
                logging.AddConsole(); // Log to console output
                logging.AddDebug();   // Log to debug output
            });
        }


        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();
            app.UseLoggingMiddleware();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseStaticFiles();


        }
    }


}


