﻿using DuolingoClone.Database;
using DuolingoClone.Entities.Progress;
using DuolingoClone.Repositories.Progress.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DuolingoClone.Repositories.Progress
{
    public class CourseRepository : ICourseRepository
    {
        private readonly MongoDBContext db;
        private IMongoCollection<EnrolledCourses> enrolledCourses;
        private FilterDefinitionBuilder<EnrolledCourses> CoursesFilterBuilder;
        private IMongoCollection<LevelProgress> levelsProgress;
        private FilterDefinitionBuilder<LevelProgress> levelProgressFilterBuilder;

        public CourseRepository(MongoDBContext _db)
        {
            db = _db;
            enrolledCourses = db.GetCollection<EnrolledCourses>("EnrolledCourse");
            CoursesFilterBuilder = Builders<EnrolledCourses>.Filter;

            levelsProgress = db.GetCollection<LevelProgress>("LevelProgress");
            levelProgressFilterBuilder = Builders<LevelProgress>.Filter;
        }


        public string AddEnrolledCourse(EnrolledCourses enrolledCourse)
        {
            enrolledCourses.InsertOne(enrolledCourse);
            return enrolledCourse.EnrolledCoursesId;
        }

        public List<EnrolledCourses> GetAllEnrolledCourses(string userId)
        {
            var filter = CoursesFilterBuilder.Eq(s => s.UserId, userId);
            return enrolledCourses.Find(filter).ToList(); 
        }

        public EnrolledCourses GetCoursebyId (string courseId)
        {
            var filter = CoursesFilterBuilder.Eq(s => s.EnrolledCoursesId, courseId);
            EnrolledCourses course = enrolledCourses.Find(filter).FirstOrDefault();
            return course;

        }
        public EnrolledCourses GetCourse(string userId, string LangId)
        {
            var filter = CoursesFilterBuilder.Eq(s => s.UserId, userId)& 
                CoursesFilterBuilder.Eq(s => s.LanguageId, LangId);
            EnrolledCourses course = enrolledCourses.Find(filter).FirstOrDefault();
            return course;

        }

        public void AddLevelProgress(LevelProgress levelProgress)
        {
            levelsProgress.InsertOne(levelProgress);

        }

        public LevelProgress GetLevelProgressById(string levelId)
        {
            var filter = levelProgressFilterBuilder.Eq(s => s.LevelId, levelId);
            return levelsProgress.Find(filter).First();
        }

        public void UpdateXp(string courseId, int xp)
        {
            EnrolledCourses course = GetCoursebyId(courseId);
            course.XpEarned += xp;
            enrolledCourses.ReplaceOne(new BsonDocument("_id", courseId), course);
        }

        public void UpdateStatus(string courseId) 
        {
            EnrolledCourses course = GetCoursebyId(courseId);
            course.status = false;

            enrolledCourses.ReplaceOne(courseId, course);
        }

        public List<LevelProgress> GetAvailableLevels (string userId , string LangId)
        {
            EnrolledCourses course = GetCourse(userId , LangId);
            string courseId = course.EnrolledCoursesId;
            var filter = levelProgressFilterBuilder.Eq(s => s.EnrolledCourseId,courseId);
            return levelsProgress.Find(filter).ToList();

        }

        public string ChallengeCompleted(LevelProgress levelProgress)
        {
            levelsProgress.ReplaceOne(new BsonDocument("_id", levelProgress.id),levelProgress );
            return "updated";
        }
    }
}
