﻿using DuolingoClone.Database;
using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Entities.Progress;
using DuolingoClone.Repositories.EndUsers;
using DuolingoClone.Repositories.EndUsers.Intefaces;
using DuolingoClone.Repositories.Progress.Interfaces;
using MongoDB.Driver;

namespace DuolingoClone.Repositories.Progress
{
    public class StreakRepository : IStreakRepository
    {
        private readonly MongoDBContext db;
        private IMongoCollection<Streak> streaks;
        private FilterDefinitionBuilder<Streak> filterBuilder;
        private IStudentRepository StudentRepository;

        public StreakRepository(MongoDBContext _db , IStudentRepository _StudentRepository)
        {
            StudentRepository = _StudentRepository;
            db = _db;
            streaks = db.GetCollection<Streak>("Streak");
            filterBuilder = Builders<Streak>.Filter;
        }

        public string AddStreak(Streak streak)
        {
            streaks.InsertOne(streak);
            StudentRepository.UpdateLastDayStreak(streak.UserId);
            return "insert";
        }

        public bool HasLearned(DateTime date, string userId)
        {
            var filter = filterBuilder.Eq(s => s.UserId, userId) &
                 filterBuilder.Eq(s => s.StreakDate, date);
            Streak streak = streaks.Find(filter).FirstOrDefault();
            if (streak is null)
            {
                return false;
            }
            return true;
        }


        public List<Streak> GetStreaks(string userId)
        {
            var filter = filterBuilder.Eq(s => s.UserId, userId);
            return streaks.Find(filter).ToList();
        }

        


    }
}
