﻿using DuolingoClone.Entities.Progress;

namespace DuolingoClone.Repositories.Progress.Interfaces
{
    public interface IStreakRepository
    {
        public string AddStreak(Streak streak);
        public bool HasLearned(DateTime date, string userId);
        public List<Streak> GetStreaks(string userId);
    }
}
