﻿using DuolingoClone.Entities.Progress;

namespace DuolingoClone.Repositories.Progress.Interfaces
{
    public interface ICourseRepository
    {
        public string AddEnrolledCourse(EnrolledCourses enrolledCourse);
        public EnrolledCourses GetCourse(string userId, string LangId);
        public void AddLevelProgress(LevelProgress levelProgress);
        public LevelProgress GetLevelProgressById(string levelId);
        public void UpdateXp(string courseId, int xp);
        public void UpdateStatus(string courseId);
        public List<EnrolledCourses> GetAllEnrolledCourses(string userId);
        public List<LevelProgress> GetAvailableLevels(string userId , string LangId);
        public string ChallengeCompleted(LevelProgress levelProgress);
    }
}
