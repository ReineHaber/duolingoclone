﻿using DuolingoClone.Entities.Lang;

namespace DuolingoClone.Repositories.Lang.Interfaces
{
    public interface IWordRepository
    {
        public void Add(Word word);
        public Word GetWordTranslation(string wordName);
    }
}
