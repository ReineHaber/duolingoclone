﻿using DuolingoClone.Entities.Lang;

namespace DuolingoClone.Repositories.Lang.Interfaces
{
    public interface ILanguageRepository
    {
        public string AddLanguage(Language language);
        public List<Language> GetAllLanguages();
        public string GetLanguageId(string languageName);
        public Language GetLanguageById(string languageId);
    }
}
