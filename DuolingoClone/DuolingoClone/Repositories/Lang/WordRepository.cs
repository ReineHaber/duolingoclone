﻿using DuolingoClone.Database;
using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Entities.Lang;
using DuolingoClone.Repositories.Lang.Interfaces;
using MongoDB.Driver;
using System.Reflection.Metadata;

namespace DuolingoClone.Repositories.Lang
{
    public class WordRepository : IWordRepository
    {
        private readonly MongoDBContext db;
        private IMongoCollection<Word> words;
        private FilterDefinitionBuilder<Word> filterBuilder;
        public WordRepository(MongoDBContext _db)
        {
            db = _db;
            words = db.GetCollection<Word>("Word");
            filterBuilder = Builders<Word>.Filter;

        }

        public void Add(Word word)
        {
            words.InsertOne(word);
        }

        public Word GetWordTranslation(string wordName)
        {
            var filter = filterBuilder.Eq(w => w.WordName, wordName);
            Word word = words.Find(filter).SingleOrDefault();
            return word;
        }
    }
}
