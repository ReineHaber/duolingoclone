﻿using DuolingoClone.Database;
using DuolingoClone.Entities.Lang;
using DuolingoClone.Repositories.Lang.Interfaces;
using MongoDB.Driver;

namespace DuolingoClone.Repositories.Lang
{
    public class LanguageRepository : ILanguageRepository
    {
        private readonly MongoDBContext db;
        private IMongoCollection<Language> languages;
        private FilterDefinitionBuilder<Language> filterBuilder;
        public LanguageRepository(MongoDBContext _db)
        {
            db = _db;
            languages = db.GetCollection<Language>("Language");
            filterBuilder = Builders<Language>.Filter;

        }

        public string AddLanguage(Language language)
        {
            languages.InsertOne(language);
            return language.LanguageId;
        }

        public List<Language> GetAllLanguages()
        {
            return languages.Find(_ => true).ToList();

        }

        public string GetLanguageId(string languageName)
        {
            var filter = filterBuilder.Eq(l => l.LanguageName, languageName);
            Language lang = languages.Find(filter).First();

            return lang.LanguageId;
        }
        public Language GetLanguageById(string languageId)
        {
            var filter = filterBuilder.Eq(l => l.LanguageId, languageId);
            Language lang = languages.Find(filter).First();

            return lang;
        }
    }
}
