﻿using DuolingoClone.Database;
using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Entities.Sections;
using DuolingoClone.Repositories.Sections.Intefaces;
using MongoDB.Driver;

namespace DuolingoClone.Repositories.Sections
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly MongoDBContext db;
        private IMongoCollection<Question> questions;
        private FilterDefinitionBuilder<Question> filterBuilder;
        public QuestionRepository(MongoDBContext _db)
        {
            db = _db;
            questions = db.GetCollection<Question>("Question");
            filterBuilder = Builders<Question>.Filter;
        }

        public string AddQuestion(Question question)
        {
            questions.InsertOne(question);
            return question.QuestionId;
        }

        public List<Question> GetAllQuestions(string challengeId)
        {
            var filter = filterBuilder.Eq(s => s.ChallengeId, challengeId);
            return questions.Find(filter).ToList();
        }
    }
}
