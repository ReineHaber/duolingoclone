﻿using DuolingoClone.Database;
using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Entities.Sections;
using DuolingoClone.Repositories.Sections.Intefaces;
using MongoDB.Driver;

namespace DuolingoClone.Repositories.Sections
{
    public class ChallengeRepository : IChallengeRepository
    {
        private readonly MongoDBContext db;
        private IMongoCollection<Challenge> challenges;
        private FilterDefinitionBuilder<Challenge> filterBuilder;
        public ChallengeRepository(MongoDBContext _db)
        {
            db = _db;
            challenges = db.GetCollection<Challenge>("Challenge");
            filterBuilder = Builders<Challenge>.Filter;
        }

        public string AddChallenge(Challenge challenge)
        {
            challenges.InsertOne(challenge);
            return challenge.ChallengeId;
        }

        public string GetChallengeId( int challengeNbr, string levelId)
        {
            var filter = filterBuilder.Eq( c => c.ChallengeNbr, challengeNbr )& 
                filterBuilder.Eq( c => c.LevelId, levelId );

            Challenge challenge = challenges.Find(filter).First();

            return challenge.ChallengeId;

        }

        public List<Challenge> getAll(string levelId)
        {
            var filter = filterBuilder.Eq(c => c.LevelId, levelId);
            return challenges.Find(filter).ToList();

        }



    }
}
