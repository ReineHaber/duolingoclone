﻿using DuolingoClone.Database;
using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Entities.Sections;
using DuolingoClone.Repositories.Sections.Intefaces;
using MongoDB.Driver;

namespace DuolingoClone.Repositories.Sections
{
    public class LevelRepository : ILevelRepository
    {
        private readonly MongoDBContext db;
        private IMongoCollection<Level> levels;
        private FilterDefinitionBuilder<Level> filterBuilder;
        public LevelRepository(MongoDBContext _db)
        {
            db = _db;
            levels = db.GetCollection<Level>("Level");
            filterBuilder = Builders<Level>.Filter;
        }

        public string AddLevel(Level level)
        {
            levels.InsertOne(level);
            return level.LevelId;
        }

        public string GetLevelId(string langId, int orderNbr)
        {
            var filter = filterBuilder.Eq(l => l.LevelOrderNbr, orderNbr) &
                filterBuilder.Eq(l => l.LanguageId, langId);
            try
            {
                Level level = levels.Find(filter).First();
                return level.LevelId;
            }
            catch(System.InvalidOperationException e)
            {
                return null;
            }
            
        }

        public string GetLevelId(string levelOrderId)
        {
            var filter = filterBuilder.Eq(l => l.LevelOrderRequired, levelOrderId);
            try
            {
                Level level = levels.Find(filter).First();
                return level.LevelId;
            }
            catch(System.InvalidOperationException e)
            {
                return null;
            }
           
        }


        public List<Level> GetLevels(string LanguageId)
        {
            try
            {
                var filter = filterBuilder.Eq(l => l.LanguageId, LanguageId);
                return levels.Find(filter).ToList();
            }catch(System.InvalidOperationException e)
            {
                return null;
            }
        }


    }
}
