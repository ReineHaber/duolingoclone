﻿using DuolingoClone.Entities.Sections;

namespace DuolingoClone.Repositories.Sections.Intefaces
{
    public interface IChallengeRepository
    {
        public string AddChallenge(Challenge challenge);
        public string GetChallengeId(int challengeNbr, string levelId);
        public List<Challenge> getAll(string levelId);
        
    }
}
