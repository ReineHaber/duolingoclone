﻿using DuolingoClone.Entities.Sections;

namespace DuolingoClone.Repositories.Sections.Intefaces
{
    public interface ILevelRepository
    {
        public string AddLevel(Level level);
        public string GetLevelId(string langId, int orderNbr);
        public string GetLevelId(string levelOrderId);
        public List<Level> GetLevels(string LanguageId);
    }
}
