﻿using DuolingoClone.Entities.Sections;

namespace DuolingoClone.Repositories.Sections.Intefaces
{
    public interface IQuestionRepository
    {
        public string AddQuestion(Question question);
        public List<Question> GetAllQuestions(string challengeId);
    }
}
