﻿using DuolingoClone.Database;
using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Repositories.EndUsers.Intefaces;
using Microsoft.AspNetCore.Identity;
using MongoDB.Driver;

namespace DuolingoClone.Repositories.EndUsers
{
    public class AdminRepository : IAdminRepository
    {
        private readonly MongoDBContext db;
        private IMongoCollection<Admin> admins;
        private FilterDefinitionBuilder<Admin> filterBuilder;
        public AdminRepository(MongoDBContext _db)
        {
            db = _db;
            admins = db.GetCollection<Admin>("Admin");
            filterBuilder = Builders<Admin>.Filter;
        }


        public string AddUser(Admin user)
        {
            admins.InsertOne(user);
            return user.UserId;
        }
    }
}
