﻿using DuolingoClone.Entities.EndUsers;

namespace DuolingoClone.Repositories.EndUsers.Intefaces
{
    public interface IAdminRepository
    {
        public string AddUser(Admin user);
    }
}
