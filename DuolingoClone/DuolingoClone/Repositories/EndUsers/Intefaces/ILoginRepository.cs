﻿using DuolingoClone.Entities.EndUsers;

namespace DuolingoClone.Repositories.EndUsers.Intefaces
{
    public interface ILoginRepository
    {
        public T? LoginUser<T>(string username, string password) where T : User;
    }
}
