﻿using DuolingoClone.Entities.EndUsers;

namespace DuolingoClone.Repositories.EndUsers.Intefaces
{
    public interface IStudentRepository
    {
        public string AddUser(Student user);
        public string GetDateOfJoin(string userid);
        public void UpdateLastDayStreak(string id);
        public bool EmailExist(string email);

    }
}
