﻿
using DuolingoClone.Database;
using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Repositories.EndUsers.Intefaces;
using Microsoft.AspNetCore.Http.HttpResults;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DuolingoClone.Repositories.EndUsers
{
    public class StudentRepository : IStudentRepository
    {
        private readonly MongoDBContext db;
        private IMongoCollection<Student> students;
        private FilterDefinitionBuilder<Student> filterBuilder;


        public StudentRepository(MongoDBContext _db)
        {
            db = _db;
            students = db.GetCollection<Student>("Student");
            filterBuilder = Builders<Student>.Filter;
        }


        public  Student GetUserById(string userid)
        {
            var filter = filterBuilder.Eq(s => s.UserId, userid);
            try
            {
                Student student = students.Find(filter).FirstOrDefault();
                return student;

            }
            catch(System.InvalidOperationException e)
            {
                return null;
            }
            
        }


        public bool EmailExist(string email)
        {
            var filter = filterBuilder.Eq(s => s.Email, email);
            try
            {
                Student s = students.Find(filter).First();
                return s == null;
            }
            catch (System.InvalidOperationException e)
            {
                return false;
            }
        }


        public string AddUser(Student user)
        {
            try
            {
                students.InsertOne(user);
                return user.UserId;
            }
            catch (System.InvalidOperationException e)
            {
                return null;
            }
            
        }


        public string GetDateOfJoin(string userid)
        {
            Student s = GetUserById(userid);
            if (s == null)
            {
                return "";

            }
            return s.DateOfJoin.ToString("dd-MM-yyyy");
        }

        public void UpdateLastDayStreak(string id)
        {

            Student s = GetUserById(id);

            if (s.LastDayStreak == null)
            {
                s.LastDayStreak = DateTime.Now;
                s.StreakNbr = 1;
                students.ReplaceOne(new BsonDocument("_id", id), s);
                return;
            }

            TimeSpan diff = (TimeSpan) (DateTime.Now - s.LastDayStreak);

            if (diff.Days <= 1)
            {
                s.StreakNbr += 1;
            }
            else
            {
                s.StreakNbr = 0;
            }

            s.LastDayStreak = DateTime.Now;
            students.ReplaceOne(new BsonDocument("_id", id), s);
             
        }
    }
}
