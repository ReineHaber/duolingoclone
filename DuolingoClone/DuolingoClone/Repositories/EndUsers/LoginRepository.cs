﻿using DuolingoClone.Database;
using DuolingoClone.Entities.EndUsers;
using DuolingoClone.Repositories.EndUsers.Intefaces;
using MongoDB.Driver;

namespace DuolingoClone.Repositories.EndUsers
{
    public class LoginRepository : ILoginRepository
    {

        private readonly MongoDBContext db;

        public LoginRepository(MongoDBContext _db)
        {
            db = _db;
        }


        public T? LoginUser<T> (string username, string password) where T : User 
        {
            try
            {
                var users = db.GetCollection<T>(typeof(T).Name);

                var filterBuilder = Builders<T>.Filter;
                var filter = filterBuilder.Eq(s => s.UserName, username) &
                    filterBuilder.Eq(s => s.Password, password);

                T user = users.Find(filter).First();

                return user;
            }
            catch(System.InvalidOperationException e)
            {
                return null;
            }
        }
    }
}
