﻿using DuolingoClone.Entities.Lang;
using DuolingoClone.Entities.Sections;
using DuolingoClone.Entities.Sections.DTOs;
using DuolingoClone.Services;
using DuolingoClone.Services.Sections.Intefaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Diagnostics.CodeAnalysis;

namespace DuolingoClone.Controllers
{
    [Route("api/v1/section")]
    [ApiController]
    public class SectionController : ControllerBase
    {

        private readonly IQuestionService questionService;
        private readonly IChallengeService challengeService;
        private readonly ILevelService levelService;



        public SectionController(IQuestionService questionService, IChallengeService challengeService, ILevelService levelService)
        {
            this.questionService = questionService;
            this.challengeService = challengeService;
            this.levelService = levelService;

        }



        [HttpPost]
        [Route("addChallenge")]

        public IActionResult AddChallenge([FromBody][NotNull] ChallengeDTO challengeDTO)
        {
            try
            {
                string id = challengeService.AddChallenge(challengeDTO.LevelId, challengeDTO.ChallengeNbr);
                return Ok(id);
            }
            catch (ItemExistsOrIsNull e)
            {
                return BadRequest(e.Message);
            }
        }





        [HttpGet]
        [Route("getChallengeId")]

        public IActionResult GetChallengeId([FromBody][NotNull] ChallengeDTO challengeDTO)
        {
            try
            {
                string id = challengeService.GetChallengeId(challengeDTO.LevelId, challengeDTO.ChallengeNbr);
                return Ok(id);
            }
            catch(ItemExistsOrIsNull e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet]
        [Route("getAllChallenges")]

        public IActionResult GetAll([FromQuery] string levelId)
        {
            List<Challenge> all = challengeService.getAll(levelId);
           if(all is null)
            {
                return NotFound("error not found");
            }
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(all);
            return Ok(json);
        }



        //[HttpPost("addLevel")]

        //public IActionResult AddLevel([FromBody][NotNull] LevelDTO levelDTO)
        //{
        //    string id = levelService.AddLevel(levelDTO.LevelOrderNbr, levelDTO.LanguageId);
        //    if (id == null)
        //    {
        //        return BadRequest("Couldn't insert");
        //    }
        //    return Ok(id);
        //}




        [HttpGet]
        [Route("getLevelId")]

        public IActionResult GetLevelId([FromBody][NotNull] LevelDTO levelDTO)
        {
            string id = levelService.GetLevelId( levelDTO.LanguageId, levelDTO.LevelOrderNbr);

            if(id == null)
            {
                return NotFound("not found!");
            }
            return Ok(id);
        }




        [HttpGet]
        [Route("getAllLevels")]

        public IActionResult GetAllLevels([FromQuery][NotNull] string langId)
        {
            List<Level> levels = levelService.GetLevels(langId);

            if (levels == null)
            {
                return NotFound("not found!");
            }
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(levels);
            return Ok(json);
        }





        [HttpGet]
        [Route("addQuestion/{ChallengeId}/{lang}")]
        public IActionResult AddQuestion([FromRoute] string ChallengeId, [FromRoute]string lang,string QuestionText, string QuestionAnswer )
        {
            string id = questionService.AddQuestion(QuestionText,QuestionAnswer, ChallengeId);


            if (id == null)
            {
                return BadRequest("Couldn't insert");
            }
            return Redirect("http://localhost/Final%20Project/questions.php?lang="+lang+"&ChalId="+ ChallengeId);
        }





        [HttpGet]
        [Route("getAllQuestions")]
            
        public IActionResult GetAllQuestions([FromQuery][NotNull] string challengeId)
        {
            List<Question> questions = questionService.GetQuestions(challengeId);

            if (questions == null)
            {
                return NotFound("not found!");
            }
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(questions);
            return Ok(json);
        }
    }
}
