﻿ using DuolingoClone.Entities.EndUsers.DTOs;
using DuolingoClone.Services;
using DuolingoClone.Services.EndUsers.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DuolingoClone.Controllers
{
    [Route("api/v1/students")]
    [ApiController]
    public class StudentController : ControllerBase
    {

        private readonly IStudentService studentService;


        public StudentController(IStudentService _studentService) 
        {
            studentService = _studentService;
        }


        [HttpGet]
        [Route("login")]

        public IActionResult login([FromBody] UserLoginDTO userDTO)
        {
            try
            {
                string id = studentService.GetStudent(userDTO.UserName, userDTO.Password);

                return Ok(id);
            }
            catch (ItemExistsOrIsNull e)
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "error while logging in");
            }
        }


        [HttpPost]
        [Route("signup")]

        public IActionResult signup([FromBody] UserDTO userDTO)
        {
            try
            {
                string token = studentService.CreateStudent(userDTO);
                return Ok(token);
            }
            catch(ItemExistsOrIsNull e)
            {
                return BadRequest(e.Message);
            }            
            
        }

    }
}
