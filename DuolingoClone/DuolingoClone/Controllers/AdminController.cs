﻿using DuolingoClone.Entities.EndUsers.DTOs;
using DuolingoClone.Services;
using DuolingoClone.Services.EndUsers.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DuolingoClone.Controllers
{

    [Route("api/v1/admins")]
    [ApiController]

    public class AdminController : ControllerBase
    {
        private IAdminService adminService;


        public AdminController(IAdminService _adminService)
        {
            adminService = _adminService;
        }



        [Route("login")]
        [HttpGet]
        
        public IActionResult Login(string UserName,  string Password )
        {
            try
            {
                string username = adminService.GetAdmin(UserName,Password);
                return Redirect("http://localhost/Final%20Project/index.php" + "?user=" + Uri.EscapeUriString(username));
            }
            catch (ItemExistsOrIsNull e)
            {
                return NotFound(e.Message);
            }
            
        }


        [Route("signup")]
        [HttpPost]
        
        public IActionResult signup([FromBody] UserDTO userDTO)
        {
            try
            {
                string id = adminService.CreateAdmin(userDTO);
                return Ok("Admin created!");
            }
            catch (ItemExistsOrIsNull e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
