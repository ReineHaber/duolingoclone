﻿using DuolingoClone.Entities.Lang;
using DuolingoClone.Entities.Lang.DTOs;
using DuolingoClone.Services.Lang.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.CodeAnalysis;
using System.Web;

using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace DuolingoClone.Controllers
{
    [Route("api/v1/language")]
    public class LanguageController : ControllerBase
    { 
        private readonly IWordService wordService;
        private readonly ILanguageService languageService;
     

        public LanguageController(IWordService wordService, ILanguageService languageService)
        {
            this.wordService = wordService;
            this.languageService = languageService;
 
        }

        [HttpPost]
        [Route("addLanguage")]

        public IActionResult AddLanguage (string Langname)
        {
            
            string id = languageService.AddLanguage(Langname);
            if(id == string.Empty)
            {
                return BadRequest("not inserted");
            }
            return Redirect("http://localhost/Final%20Project/index.php");
        }

        [HttpGet]
        [Route("getAllLang")]

        public IActionResult GetAll()
        {
            List<Language> languages = languageService.GetAll();
            if(languages == null)
            {
                return NotFound("no languages");
            }
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(languages);
            return Ok(json) ;
        }

        [HttpGet]
        [Route("getLangById")]

        public IActionResult GetLangByid(string langId)
        {
            Language lang = languageService.GetLanguageById(langId);
            if(lang == null)
            {
                return NotFound("not found");
            }
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(lang);
            return Ok(json);
        }



        [HttpGet]
        [Route("getTranslation")]

        public IActionResult GetTranslation([FromQuery]string wordName)
        {
            string translation = wordService.GetTranslation(wordName);
            if(translation == "")
            {
                return NotFound("word not found");
            }
            return Ok(translation);
        }

        [HttpGet]
        [Route("addWordFile/{LangId}")]

        public IActionResult AddWordsFile(IFormFile file, [FromRoute] string LangId)
        {
            if (file == null)
            {
                return BadRequest("file is empty!");

            }
            wordService.AddWords(file, LangId);
            return Ok("file uploaded");

        }
        

        
    }
}
