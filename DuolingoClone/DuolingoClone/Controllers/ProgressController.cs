﻿using DuolingoClone.Entities.Progress;
using DuolingoClone.Entities.Progress.DTOs;
using DuolingoClone.Services.Progress.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DuolingoClone.Controllers
{
    [Route("api/v1/progress")]
    [ApiController]
    public class ProgressController : ControllerBase
    {
        private readonly IStreakService streakService;
        private readonly ICourseService courseService;

        public ProgressController(IStreakService _streakService, ICourseService _courseService)
        {
            streakService = _streakService;
            courseService = _courseService;
        }

        [HttpPost]
        [Route("AddCourse")]
        public IActionResult AddCourse([FromBody]EnrolledCourseDTO enrolledCoursesDTO)
        {
            string id = courseService.AddCourse(enrolledCoursesDTO.UserId, enrolledCoursesDTO.LanguageId);
            if (id == null)
            {
                return BadRequest("couldn't create");
            }

            return Ok(id);
        }

        [HttpGet]
        [Route("GetCourse")]
        public IActionResult GetCourse(string userId , string lang)
        {
            EnrolledCourses enrolledCourse = courseService.GetCourse(userId,lang);
            if(enrolledCourse == null)
            {
                return NotFound("course not found");
            }
            return Ok(enrolledCourse);
        }


        [HttpPost]
        [Route("AddXp")]
        public IActionResult UpdateXp(string courseId, int xp)
        {
            string message = courseService.UpdateXp(courseId,xp);
            return Ok(message);
        }


        [HttpPost]
        [Route("Deletecourse")]
        public IActionResult UnEnrollCourse(string courseId)
        {
            string message = courseService.UpdateStatus(courseId);
            return Ok(message);
        }


        [HttpGet]
        [Route("AvailableLevel")]
        public IActionResult availableLevels(string userId , string LangId)
        {
            List<levelAvailableDTO> levels = courseService.GetLevels(userId , LangId);
            return Ok(levels);
        }


        [HttpGet]
        [Route("GetallEnrolledCourses")]
        public IActionResult GetAllEnrolledCourses(string userId)
        {
            Dictionary<string, string> courses = courseService.GetAllEnrolledCourses(userId);
            return Ok(courses);
        }


        [HttpPost]
        [Route("ChallengeIsCompleted")]
        public IActionResult ChallengeCompleted(string levelId, string courseId)
        {
            string message = courseService.ChallengeCompleted(levelId,courseId);
            return Ok(message);
        }

        [HttpPost]
        [Route("addStreak/{UserId}")]

        public IActionResult AddStreak ([FromRoute]string UserId)
        {
            string message = streakService.CreateStreak(UserId);
            return Ok(message);
        }

        [HttpGet]
        [Route("getStreaks/{UserId}")]

        public IActionResult getStreak([FromRoute] string UserId)
        {
            List<Streak> streaks = streakService.GetAllStreaks(UserId);
            if (streaks is null)
            {
                return BadRequest("no streaks");
            }
            return Ok(streaks);
        }


    }
}
