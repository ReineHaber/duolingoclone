﻿using MongoDB.Bson.Serialization.Attributes;

namespace DuolingoClone.Entities.Lang
{
    public class Language
    {
        [BsonId]
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        //public string flag { get; set; }

        public Language()
        {
            LanguageId = Guid.NewGuid().ToString();
        }
    }
}
