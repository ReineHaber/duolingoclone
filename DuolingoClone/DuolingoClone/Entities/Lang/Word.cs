﻿using MongoDB.Bson.Serialization.Attributes;

namespace DuolingoClone.Entities.Lang
{
    public class Word
    {
        [BsonId]
        public string WordId { get; set; }
        public string WordName { get; set; }
        public string TranslationToEng { get; set; }
        public string LanguageId { get; set; }

        public Word()
        {
            WordId = Guid.NewGuid().ToString();
        }

    }
}
