﻿using MongoDB.Bson.Serialization.Attributes;

namespace DuolingoClone.Entities.Sections
{
    public class Level
    {
        [BsonId]
        public string LevelId { get; set; } = Guid.NewGuid().ToString();
        public string LanguageId { get; set; }
        public int LevelOrderNbr { get; set; }
        public string LevelOrderRequired { get; set; }
    }
}
