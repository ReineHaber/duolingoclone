﻿namespace DuolingoClone.Entities.Sections.DTOs
{
    public class LevelDTO
    {
        public string LanguageId { get; set; }
        public int LevelOrderNbr { get; set; }
    }
}
