﻿namespace DuolingoClone.Entities.Sections.DTOs
{
    public class ChallengeDTO
    {
        public int ChallengeNbr { get; set; }
        public string LevelId { get; set; }
    }
}
