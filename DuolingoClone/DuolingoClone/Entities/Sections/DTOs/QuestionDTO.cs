﻿namespace DuolingoClone.Entities.Sections.DTOs
{
    public class QuestionDTO
    {
        public string QuestionText { get; set; }
        public string QuestionAnswer { get; set; }
    }
}
