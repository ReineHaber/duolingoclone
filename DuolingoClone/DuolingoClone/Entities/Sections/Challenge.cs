﻿using MongoDB.Bson.Serialization.Attributes;

namespace DuolingoClone.Entities.Sections
{
    public class Challenge
    { 

        [BsonId]
        public string ChallengeId { get; set; }
        public int ChallengeNbr { get; set; }
        public string LevelId { get; set; }

        public Challenge()
        {
            ChallengeId = Guid.NewGuid().ToString();
        }
    }
}
