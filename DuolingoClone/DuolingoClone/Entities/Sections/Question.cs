﻿using MongoDB.Bson.Serialization.Attributes;

namespace DuolingoClone.Entities.Sections
{
    public class Question
    {
        [BsonId]
        public string QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string QuestionAnswer { get; set; }
        public string ChallengeId { get; set; }

        public Question()
        {
            QuestionId = Guid.NewGuid().ToString();
        }
    }
}
