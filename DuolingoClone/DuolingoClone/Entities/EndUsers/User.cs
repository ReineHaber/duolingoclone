﻿using MongoDB.Bson.Serialization.Attributes;

namespace DuolingoClone.Entities.EndUsers
{
    public class User
    {
        [BsonId]
        public string UserId { get; set; } = Guid.NewGuid().ToString();
        [BsonElement]
        public string UserName { get; set; }
        [BsonElement]
        public string Password { get; set; }
        [BsonElement]
        public string Email { get; set; }
    }
}
