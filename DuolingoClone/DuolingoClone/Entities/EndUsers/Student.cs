﻿namespace DuolingoClone.Entities.EndUsers
{
    public class Student : User
    {
        public DateTime DateOfJoin { get; set; }
        public DateTime? LastDayStreak { get; set; }
        public int StreakNbr { get; set; }


        public Student()
        {
            DateOfJoin = DateTime.Today;
            LastDayStreak = null;
            StreakNbr = 0;
        }
    }
}
