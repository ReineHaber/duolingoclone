﻿using Microsoft.AspNetCore.Identity;
using System.Globalization;

namespace DuolingoClone.Entities.EndUsers
{
    public class Admin : User
    {
        public string role { get; set; }
        public Admin()
        {
            role = "admin";
        }
    }

}