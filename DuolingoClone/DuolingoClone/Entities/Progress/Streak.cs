﻿using MongoDB.Bson.Serialization.Attributes;

namespace DuolingoClone.Entities.Progress
{
    public class Streak
    {

        [BsonId]
        public string id { get; set; }
        public string UserId { get; set; }
        public DateTime StreakDate { get; set; }
        public bool HasLearned { get; set; }

        public Streak()
        {
            id = Guid.NewGuid().ToString();
            StreakDate = DateTime.Now;
            HasLearned = true;
        }

    }
}
