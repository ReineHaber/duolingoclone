﻿namespace DuolingoClone.Entities.Progress.DTOs
{
    public class LevelProgressDTO
    {
        public string LevelId { get; set; }
        public string EnrolledCourseId { get; set; }
    }
}
