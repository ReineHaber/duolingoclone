﻿namespace DuolingoClone.Entities.Progress.DTOs
{
    public class EnrolledCourseDTO
    {
        public string UserId { get; set; }
        public string LanguageId { get; set; }
    }
}
