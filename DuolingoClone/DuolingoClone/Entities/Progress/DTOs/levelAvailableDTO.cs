﻿namespace DuolingoClone.Entities.Progress.DTOs
{
    public class levelAvailableDTO
    {
        public string LevelId { get; set; }
        public int CurrentChallengeNbr { get; set; }
        public bool IsFinished { get; set; }
    }
}
