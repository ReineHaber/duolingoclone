﻿using MongoDB.Bson.Serialization.Attributes;

namespace DuolingoClone.Entities.Progress
{
    public class EnrolledCourses
    {
        [BsonId]
        public string EnrolledCoursesId { get; set; }
        public string UserId { get; set; }
        public string LanguageId { get; set; }
        public int XpEarned { get; set; }
        public bool status { get; set; }

        public EnrolledCourses()
        {
            EnrolledCoursesId = Guid.NewGuid().ToString();
            XpEarned = 0;
            status = true;
        }
    }
}