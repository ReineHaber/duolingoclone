﻿using MongoDB.Bson.Serialization.Attributes;

namespace DuolingoClone.Entities.Progress
{
    public class LevelProgress
    {
        [BsonId]
        public string id { get; set; }
        public string LevelId { get; set; }
        public string EnrolledCourseId { get; set; }
        public int CurrentChallengeNbr { get; set; }
        public bool IsFinished { get; set; }    

        public LevelProgress()
        {
            id = Guid.NewGuid().ToString();
            CurrentChallengeNbr = 1;
            IsFinished = false;
        }
    }
}
